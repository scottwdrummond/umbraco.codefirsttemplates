﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Functions.cs" company="Multiply">
//   Multiply 2013
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace CodeFirstTemplates
{
    using System.IO;
    using System.Text.RegularExpressions;
    using System.Web;

    /// <summary>
    ///     The functions.
    /// </summary>
    public class Functions
    {
        #region Public Methods and Operators

        /// <summary>
        ///     The is render mode MVC.
        /// </summary>
        /// <returns>
        ///     The <see cref="bool" />.
        /// </returns>
        public static bool IsRenderModeMvc()
        {
            var umbracoSettingsFile = HttpContext.Current.Server.MapPath(@"~/Config/umbracoSettings.config");
            string config = File.ReadAllText(umbracoSettingsFile);

            const string Regex = "<defaultRenderingEngine>([^<]*)</defaultRenderingEngine>";

            const RegexOptions Options =
                (RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline) | RegexOptions.IgnoreCase;
            var reg = new Regex(Regex, Options);

            // Get text from template in  Layout = "xxxxxxx";
            string mode = reg.Match(config).Groups[1].Value.ToUpper();

            return mode == "MVC";
        }

        #endregion
    }
}