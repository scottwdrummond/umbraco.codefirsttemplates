﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RegisterEvents.cs" company="Multiply">
//   Multiply 2013
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace CodeFirstTemplates
{
    using System;
    using System.Reflection;

    using umbraco.cms.presentation.Trees;

    using Umbraco.Core;
    using Umbraco.Core.Logging;

    /// <summary>
    ///     The register events.
    /// </summary>
    public class RegisterEvents : IApplicationEventHandler
    {
        #region Public Methods and Operators

        /// <summary>
        /// The on application initialized.
        /// </summary>
        /// <param name="umbracoApplication">
        /// The umbraco application.
        /// </param>
        /// <param name="applicationContext">
        /// The application context.
        /// </param>
        public void OnApplicationInitialized(
            UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        /// <summary>
        /// The on application started.
        /// </summary>
        /// <param name="umbracoApplication">
        /// The umbraco application.
        /// </param>
        /// <param name="applicationContext">
        /// The application context.
        /// </param>
        public void OnApplicationStarted(
            UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
            // Hook to add templates when "Reload Nodes" is called om the templates folder
            BaseTree.BeforeNodeRender += this.BaseTree_BeforeNodeRender;
        }

        /// <summary>
        /// The on application starting.
        /// </summary>
        /// <param name="umbracoApplication">
        /// The umbraco application.
        /// </param>
        /// <param name="applicationContext">
        /// The application context.
        /// </param>
        public void OnApplicationStarting(
            UmbracoApplicationBase umbracoApplication, ApplicationContext applicationContext)
        {
        }

        #endregion

        #region Methods

        /// <summary>
        /// The base tree_ before node render.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="node">
        /// The node.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        private void BaseTree_BeforeNodeRender(ref XmlTree sender, ref XmlTreeNode node, EventArgs e)
        {
            // The node.NodeType will return "content" or "media", etc.,
            // ...and if node.Menu is null, then there is no right-click menu to customize (such as in a Content Picker data type)
            // we only want the "templates" nodeTpe
            if (node.NodeType != "templates" || node.Menu == null)
            {
                return;
            }

            LogHelper.Info(MethodBase.GetCurrentMethod().DeclaringType, "MULTIPLY:  templates are about to be added.");

            // Check type of node, currently we have access to the icon, which could be used as an identifier!
            // Alternativily use the node.NodeID (note, NodeId here is returned as a string) to calculate the doctype 
            var cft = new CodeFirstTemplates();
            int templatesupdated = cft.RefreshTemplates();

            LogHelper.Info(MethodBase.GetCurrentMethod().DeclaringType, "MULTIPLY: " + templatesupdated + "  templates have been added.");
        }

        #endregion
    }
}