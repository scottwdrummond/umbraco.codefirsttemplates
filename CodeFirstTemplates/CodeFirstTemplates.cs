﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="CodeFirstTemplates.cs" company="Multiply">
//   Multiply 2013
// </copyright>
// --------------------------------------------------------------------------------------------------------------------
namespace CodeFirstTemplates
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text.RegularExpressions;
    using System.Web;

    using umbraco.BusinessLogic;
    using umbraco.cms.businesslogic.template;

    using Umbraco.Core.Configuration;

    /// <summary>
    ///     The code first templates.
    /// </summary>
    public class CodeFirstTemplates
    {
        #region Constants

        /// <summary>
        ///     The folder separator.
        /// </summary>
        private const char FolderSeparator = '\\';

        #endregion

        #region Fields

        /// <summary>
        ///     The templates in files.
        /// </summary>
        private readonly List<string> templatesInFiles = new List<string>();

        /// <summary>
        ///     The is render mode MVC.
        /// </summary>
        private bool isRenderModeMvc;

        /// <summary>
        ///     The root path.
        /// </summary>
        private string rootPath;

        /// <summary>
        ///     The template extension.
        /// </summary>
        private string templateExtension = ".cshtml";

        /// <summary>
        ///     The templates in database.
        /// </summary>
        private List<Template> templatesInDatabase = new List<Template>();

        #endregion

        #region Public Methods and Operators

        /// <summary>
        ///     The refresh templates.
        /// </summary>
        /// <exception>
        ///     <cref>Exception</cref>
        /// </exception>
        /// <returns>
        ///     The <see cref="int" />.
        /// </returns>
        public int RefreshTemplates()
        {
            this.isRenderModeMvc = Functions.IsRenderModeMvc();

            DirectoryInfo templatesFolder;
            if (this.isRenderModeMvc)
            {
                templatesFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath("/Views/"));
            }
            else
            {
                templatesFolder = new DirectoryInfo(HttpContext.Current.Server.MapPath("/Masterpages/"));
            }

            this.templatesInDatabase = Template.GetAllAsList();
            this.rootPath = templatesFolder.FullName;
            this.AddToAliasList(templatesFolder, this.templatesInFiles);

            int templatesAdded = 0;
            foreach (string alias in this.templatesInFiles)
            {
                this.SaveNewTemplate(alias);
                templatesAdded++;
            }

            return templatesAdded;
        }

        #endregion

        #region Methods

        /// <summary>
        /// The remove "UMB".
        /// </summary>
        /// <param name="templateName">
        /// The template name.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private static string RemoveUmb(string templateName)
        {
            // remove "umb" from template names - mew in v7
            if (UmbracoVersion.Current.Major >= 7)
            {
                if (templateName.StartsWith("umb"))
                {
                    templateName = templateName.Substring(3);
                }
            }

            return templateName;
        }

        /// <summary>
        /// The add to alias list.
        /// </summary>
        /// <param name="currentDir">
        /// The current directory.
        /// </param>
        /// <param name="list">
        /// The list.
        /// </param>
        /// <param name="isRoot">
        /// is root this the root directory.
        /// </param>
        private void AddToAliasList(DirectoryInfo currentDir, List<string> list, bool isRoot = true)
        {
            List<FileInfo> files = currentDir.GetFiles("*" + this.templateExtension).ToList();
            foreach (FileInfo file in files)
            {
                string partName = file.FullName.Replace(this.rootPath, string.Empty)
                                      .Replace(this.templateExtension, string.Empty);
                if (!isRoot)
                {
                    partName = FolderSeparator + partName;
                }

                if (this.templatesInDatabase.All(t => t.Alias != partName))
                {
                    list.Add(partName);
                }
            }

            List<DirectoryInfo> folders = currentDir.GetDirectories().ToList();
            foreach (DirectoryInfo folder in folders)
            {
                // dont add MVC  Partials and MacroPartials
                if (!(this.isRenderModeMvc && (folder.Name == "Partials" || folder.Name == "MacroPartials")))
                {
                    this.AddToAliasList(folder, this.templatesInFiles, false);
                }
            }
        }

        /// <summary>
        /// The get master.
        /// </summary>
        /// <param name="text">
        /// The text.
        /// </param>
        /// <returns>
        /// The <see cref="string"/>.
        /// </returns>
        private string GetMaster(string text)
        {
            string regex;
            if (this.isRenderModeMvc)
            {
                regex = "Layout\\s*=\\s*\"([^\"]*)\"";
                this.templateExtension = ".cshtml";
            }
            else
            {
                regex = "MasterPageFile\\s*=\\s*\"([^\"]*)\""; // ~/umbraco/masterpages/default.master";
                this.templateExtension = ".master";
            }

            const RegexOptions Options =
                (RegexOptions.IgnorePatternWhitespace | RegexOptions.Multiline) | RegexOptions.IgnoreCase;
            var reg = new Regex(regex, Options);

            // Get text from template 
            string masternameFull = reg.Match(text).Groups[1].Value;

            if (string.IsNullOrWhiteSpace(masternameFull)
                || masternameFull.Contains("/umbraco/masterpages/default.master"))
            {
                return string.Empty;
            }

            // remove extension and any path information
            masternameFull = masternameFull.Replace('/', '\\');
            masternameFull = masternameFull.Replace(this.templateExtension, string.Empty);
            string mastername = masternameFull.Split(FolderSeparator).Last();

            return mastername;
        }

        /// <summary>
        /// The save new template.
        /// </summary>
        /// <param name="alias">
        /// The alias.
        /// </param>
        private void SaveNewTemplate(string alias)
        {
            string templateName = alias.Split(FolderSeparator).Last();
            string design = File.ReadAllText(this.rootPath + alias + this.templateExtension);
            string masterName = this.GetMaster(design);
            Template master = Template.GetByAlias(masterName);
            Template newTemplate = Template.MakeNew(templateName, User.GetCurrent(), master);
            newTemplate.Text = RemoveUmb(templateName);
            newTemplate.Alias = alias;
            newTemplate.Design = design;
            newTemplate.Save();
        }

        #endregion
    }
}